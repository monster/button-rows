# window.py
#
# Copyright 2024 Jamie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from typing import Any

from gi.repository import Adw, Gtk


@Gtk.Template(resource_path="/garden/jamie/ButtonRows/gtk/window.ui")
class ButtonRowsWindow(Adw.ApplicationWindow):
    __gtype_name__ = "ButtonRowsWindow"

    toast_overlay: Adw.ToastOverlay = Gtk.Template.Child()
    list_box_cats: Gtk.ListBox = Gtk.Template.Child()
    button_row_add_cat: Adw.ButtonRow = Gtk.Template.Child()
    dialog_add_cat: Adw.AlertDialog = Gtk.Template.Child()
    entry_row_name: Adw.EntryRow = Gtk.Template.Child()

    def __init__(self, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.cats = 0

        self.name = False

    def __send_toast(self, message: str) -> None:
        toast = Adw.Toast.new(message)
        toast.set_priority(Adw.ToastPriority.HIGH)
        self.toast_overlay.add_toast(toast)

    @Gtk.Template.Callback()
    def _has_name(self, *_args: Any) -> None:
        if self.entry_row_name.get_text_length() > 0:
            self.dialog_add_cat.set_response_enabled("add", True)
            self.name = True
        else:
            self.dialog_add_cat.set_response_enabled("add", False)
            self.name = False

    def __change_icon(self) -> None:
        self.button_row_add_cat.set_start_icon_name(
            "cat-symbolic"
            if self.button_row_add_cat.get_index() == 0
            else "list-add-symbolic"
        )

    def __insert_to_list(self, row: Adw.ActionRow, index: int) -> None:
        self.list_box_cats.insert(row, index)
        self.__change_icon()

    def __remove_from_list(self, row: Adw.ActionRow) -> None:
        self.list_box_cats.remove(row)
        self.__change_icon()

    @Gtk.Template.Callback()
    def _on_button_pet_clicked(self, *_args: Any) -> None:
        match self.cats:
            case 0:
                message = _("No meows")
            case 1:
                message = _("Meow")
            case _:
                message = f'{str(self.cats)} {_("meows")}'

        self.__send_toast(message)

    @Gtk.Template.Callback()
    def _on_button_row_clear_cats_activated(self, *_args: Any) -> None:
        while self.list_box_cats.get_row_at_index(1):
            self.__remove_from_list(self.list_box_cats.get_row_at_index(0))

        match self.cats:
            case 0:
                message = _("No cats")
            case 1:
                message = _("1 cat cleared")
            case _:
                message = f'{str(self.cats)} {_("cats cleared")}'

        self.__send_toast(message)
        self.cats = 0
        self.__change_icon()

    @Gtk.Template.Callback()
    def _on_button_row_add_cat_activated(self, *_args: Any) -> None:
        self.dialog_add_cat.choose(self, None, self.__on_response_dialog_add_cat)

    def __dialog_handle_response(self, response: str, name: str) -> None:
        if response != "add":
            return

        self.__add_cat_row(name)

    @Gtk.Template.Callback()
    def _on_entry_row_name_activated(self, *_args: Any):
        if self.name:
            self.dialog_add_cat.emit("response", "add")
            self.dialog_add_cat.close()

    def __on_response_dialog_add_cat(
        self, dialog: Adw.AlertDialog, button: Gtk.Button
    ) -> None:
        self.__dialog_handle_response(
            dialog.choose_finish(button),
            self.entry_row_name.get_text(),
        )

    def __add_cat_row(self, name: str) -> None:
        self.cat_row = Adw.ActionRow(title=name)
        self.cat_row.add_suffix(
            Gtk.Image.new_from_icon_name("user-trash-symbolic"),
        )

        self.cat_row.connect("activated", self.__on_cat_row_activated)
        self.cat_row.set_activatable(True)

        self.__insert_to_list(self.cat_row, self.button_row_add_cat.get_index())
        self.cats += 1

    def __on_cat_row_activated(self, button_row: Adw.ButtonRow) -> None:
        self.__remove_from_list(button_row)
        self.cats -= 1
